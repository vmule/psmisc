# German translation for PSmisc programs.
# Copyright (C) 2002, 2007 Free Software Foundation, Inc.
# This file is distributed under the same license as the psmisc package.
# Wolfgang Schorer <wcc@wolfgangs.com>, 2002.
# Benno Schulenberg <benno@vertaalt.nl>, 2007.
# Roland Illig <roland.illig@gmx.de>, 2009, 2010, 2012, 2013, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: psmisc 22.21-pre2\n"
"Report-Msgid-Bugs-To: csmall@enc.com.au\n"
"POT-Creation-Date: 2015-06-30 23:01+1000\n"
"PO-Revision-Date: 2014-02-22 15:59+0100\n"
"Last-Translator: Roland Illig <roland.illig@gmx.de>\n"
"Language-Team: German <translation-team-de@lists.sourceforge.net>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.6.4\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: src/fuser.c:123
#, fuzzy, c-format
msgid ""
"Usage: fuser [-fMuvw] [-a|-s] [-4|-6] [-c|-m|-n SPACE] [-k [-i] [-SIGNAL]] "
"NAME...\n"
"       fuser -l\n"
"       fuser -V\n"
"Show which processes use the named files, sockets, or filesystems.\n"
"\n"
"  -a,--all              display unused files too\n"
"  -i,--interactive      ask before killing (ignored without -k)\n"
"  -I,--inode            use always inodes to compare files\n"
"  -k,--kill             kill processes accessing the named file\n"
"  -l,--list-signals     list available signal names\n"
"  -m,--mount            show all processes using the named filesystems or "
"block device\n"
"  -M,--ismountpoint     fulfill request only if NAME is a mount point\n"
"  -n,--namespace SPACE  search in this name space (file, udp, or tcp)\n"
"  -s,--silent           silent operation\n"
"  -SIGNAL               send this signal instead of SIGKILL\n"
"  -u,--user             display user IDs\n"
"  -v,--verbose          verbose output\n"
"  -w,--writeonly        kill only processes with write access\n"
"  -V,--version          display version information\n"
msgstr ""
"Aufruf: fuser [-fMuvw] [-a|-s] [-4|-6] [-c|-m|-n RAUM] [-k [-i] [-SIGNAL]] "
"NAME...\n"
"        fuser -l\n"
"        fuser -V\n"
"Zeigt an, welche Prozesse die angegebenen Dateien, Sockets oder Dateisysteme "
"benutzen.\n"
"\n"
"  -a,--all             zeige auch die ungenutzten Dateien an\n"
"  -i,--interactive     vor dem Abschießen nachfragen (ohne -k wirkungslos)\n"
"  -k,--kill            schieße Prozesse ab, die auf die angegebene Datei "
"zugreifen\n"
"  -l,--list-signals    liste die Signalnamen auf\n"
"  -m,--mount           zeige alle Prozesse an, die auf die angegebenen "
"Dateisysteme oder Blockgeräte zugreifen\n"
"  -M,--ismountpoint    Operation nur durchführen, wenn NAME ein "
"Einhängepunkt ist\n"
"  -n,--namespace RAUM  suche in angegebenem Namensraum (file, udp oder tcp)\n"
"  -s,--silent          stille Operation\n"
"  -SIGNAL              sende 'SIGNAL' anstatt SIGKILL\n"
"  -u,--user            zeige die Benutzer-IDs an\n"
"  -v,--verbose         ausführliche Ausgabe\n"
"  -w,--writeonly       nur Prozesse mit Schreibzugriff beenden\n"
"  -V,--version         zeige Versionsinformationen an\n"

#: src/fuser.c:141
#, c-format
msgid ""
"  -4,--ipv4             search IPv4 sockets only\n"
"  -6,--ipv6             search IPv6 sockets only\n"
msgstr ""
"  -4,--ipv4             suche nur IPv4-Sockets\n"
"  -6,--ipv6             suche nur IPv6-Sockets\n"

#: src/fuser.c:144
#, c-format
msgid ""
"  -                     reset options\n"
"\n"
"  udp/tcp names: [local_port][,[rmt_host][,[rmt_port]]]\n"
"\n"
msgstr ""
"  -                     Zurücksetzen der Optionen\n"
"\n"
"  udp/tcp-Namen: [lokaler_port][,[entfernter_rechner][,[entfernter_port]]]\n"
"\n"

#: src/fuser.c:151
#, c-format
msgid "fuser (PSmisc) %s\n"
msgstr "fuser (PSmisc) %s\n"

#: src/fuser.c:154
#, c-format
msgid ""
"Copyright (C) 1993-2010 Werner Almesberger and Craig Small\n"
"\n"
msgstr ""
"Copyright (C) 1993-2010 Werner Almesberger und Craig Small\n"
"\n"

#: src/fuser.c:156 src/killall.c:702 src/peekfd.c:114 src/prtstat.c:68
#: src/pstree.c:1054
#, c-format
msgid ""
"PSmisc comes with ABSOLUTELY NO WARRANTY.\n"
"This is free software, and you are welcome to redistribute it under\n"
"the terms of the GNU General Public License.\n"
"For more information about these matters, see the files named COPYING.\n"
msgstr ""
"Für PSmisc gibt es KEINERLEI GARANTIE.\n"
"Es ist freie Software und Sie dürfen sie gern gemäß den Bedingungen\n"
"der GNU General Public License (GPL) weiter vertreiben.\n"
"Zusätzliche Informationen dazu finden Sie in der Datei namens COPYING.\n"

#: src/fuser.c:175
#, c-format
msgid "Cannot open /proc directory: %s\n"
msgstr "Verzeichnis /proc kann nicht geöffnet werden: %s\n"

#: src/fuser.c:366 src/fuser.c:419 src/fuser.c:1874
#, c-format
msgid "Cannot allocate memory for matched proc: %s\n"
msgstr "Kein Speicher mehr verfügbar für zugehöriges Prozess: %s\n"

#: src/fuser.c:446
#, c-format
msgid "Specified filename %s does not exist.\n"
msgstr "Angegebener Dateiname %s existiert nicht.\n"

#: src/fuser.c:449
#, c-format
msgid "Cannot stat %s: %s\n"
msgstr "Kann Status von \"%s\" nicht ermitteln: %s\n"

#: src/fuser.c:586
#, c-format
msgid "Cannot resolve local port %s: %s\n"
msgstr "Kann lokalen Port %s nicht auflösen: %s\n"

#: src/fuser.c:604
#, c-format
msgid "Unknown local port AF %d\n"
msgstr "Unbekannter lokaler Port AF %d\n"

#: src/fuser.c:692
#, c-format
msgid "Cannot open protocol file \"%s\": %s\n"
msgstr "Kann Protokolldatei »%s« nicht öffnen: %s\n"

#: src/fuser.c:878
#, c-format
msgid "Specified filename %s is not a mountpoint.\n"
msgstr "Der angegebene Dateiname »%s« ist kein Einhängepunkt.\n"

#: src/fuser.c:978
#, c-format
msgid "%s: Invalid option %s\n"
msgstr "%s: Ungültige Option %s\n"

#: src/fuser.c:1035
msgid "Namespace option requires an argument."
msgstr "Die Namensraum-Option benötigt ein Argument."

#: src/fuser.c:1053
msgid "Invalid namespace name"
msgstr "Ungültiger Namensraum"

#: src/fuser.c:1118
msgid "You can only use files with mountpoint options"
msgstr "Dateien können nur mit der Einhängepunkt-Option verwendet werden"

#: src/fuser.c:1167
msgid "No process specification given"
msgstr "Keine Prozessspezifikation angegeben"

#: src/fuser.c:1179
msgid "all option cannot be used with silent option."
msgstr "Option -a kann nicht mit der Option -s kombiniert werden."

#: src/fuser.c:1184
msgid "You cannot search for only IPv4 and only IPv6 sockets at the same time"
msgstr ""
"Sie können nicht gleichzeitig ausschließlich nach IPv4 und ausschließlich "
"nach IPv6-Sockets suchen."

#: src/fuser.c:1263
#, c-format
msgid "%*s USER        PID ACCESS COMMAND\n"
msgstr "%*s BEN.        PID ZUGR.  BEFEHL\n"

#: src/fuser.c:1296 src/fuser.c:1353
msgid "(unknown)"
msgstr "(unbekannt)"

#: src/fuser.c:1432 src/fuser.c:1471
#, c-format
msgid "Cannot stat file %s: %s\n"
msgstr "Status der Datei %s kann nicht ermittelt werden: %s\n"

#: src/fuser.c:1557
#, c-format
msgid "Cannot open /proc/net/unix: %s\n"
msgstr "/proc/net/unix kann nicht geöffnet werden: %s\n"

#: src/fuser.c:1633
#, c-format
msgid "Kill process %d ? (y/N) "
msgstr "Prozess %d abbrechen? (y/N) "

#: src/fuser.c:1669
#, c-format
msgid "Could not kill process %d: %s\n"
msgstr "Kann Prozess %d nicht abbrechen: %s\n"

#: src/fuser.c:1684
#, c-format
msgid "Cannot open a network socket.\n"
msgstr "Kann keine Netzwerkverbindung öffnen.\n"

#: src/fuser.c:1688
#, c-format
msgid "Cannot find socket's device number.\n"
msgstr "Kann die Gerätenummer der Sockets nicht finden.\n"

#: src/killall.c:106
#, c-format
msgid "Kill %s(%s%d) ? (y/N) "
msgstr "%s(%s%d) abbrechen? (y/N) "

#: src/killall.c:109
#, c-format
msgid "Signal %s(%s%d) ? (y/N) "
msgstr "Signal %s(%s%d) senden? (y/N) "

#: src/killall.c:216
#, c-format
msgid "killall: Cannot get UID from process status\n"
msgstr "killall: Kann keine UID vom Prozessstatus erhalten\n"

#: src/killall.c:242
#, c-format
msgid "killall: Bad regular expression: %s\n"
msgstr "killall: Ungültiger regulärer Ausdruck: %s\n"

#: src/killall.c:376
#, c-format
msgid "killall: skipping partial match %s(%d)\n"
msgstr "killall: Überspringe teilweise Übereinstimmung von %s(%d)\n"

#: src/killall.c:603
#, c-format
msgid "Killed %s(%s%d) with signal %d\n"
msgstr "%s(%s%d) mit Signal %d beendet\n"

#: src/killall.c:623
#, c-format
msgid "%s: no process found\n"
msgstr "%s: Kein Prozess gefunden\n"

#: src/killall.c:664
#, c-format
msgid ""
"Usage: killall [-Z CONTEXT] [-u USER] [ -eIgiqrvw ] [ -SIGNAL ] NAME...\n"
msgstr ""
"Aufruf: killall [-Z CONTEXT] [-u USER] [ -eIgiqrvw ] [-SIGNAL] NAME...\n"

#: src/killall.c:667
#, c-format
msgid "Usage: killall [OPTION]... [--] NAME...\n"
msgstr "Aufruf: killall - [OPTION]... [--]NAME...\n"

#: src/killall.c:670
#, c-format
msgid ""
"       killall -l, --list\n"
"       killall -V, --version\n"
"\n"
"  -e,--exact          require exact match for very long names\n"
"  -I,--ignore-case    case insensitive process name match\n"
"  -g,--process-group  kill process group instead of process\n"
"  -y,--younger-than   kill processes younger than TIME\n"
"  -o,--older-than     kill processes older than TIME\n"
"  -i,--interactive    ask for confirmation before killing\n"
"  -l,--list           list all known signal names\n"
"  -q,--quiet          don't print complaints\n"
"  -r,--regexp         interpret NAME as an extended regular expression\n"
"  -s,--signal SIGNAL  send this signal instead of SIGTERM\n"
"  -u,--user USER      kill only process(es) running as USER\n"
"  -v,--verbose        report if the signal was successfully sent\n"
"  -V,--version        display version information\n"
"  -w,--wait           wait for processes to die\n"
msgstr ""
"       killall -l, --list\n"
"       killall -V, --version\n"
"\n"
"  -e,--exact          verlange genaue Übereinstimmung für sehr lange Namen\n"
"  -I,--ignore-case    Groß- und Kleinschreibung nicht beachten\n"
"  -g,--process-group  breche Prozessgruppe statt Einzelprozess ab\n"
"  -y,--younger-than   Prozesse jünger als ZEIT abbrechen\n"
"  -o,--older-than     Prozesse älter als ZEIT abbrechen\n"
"  -i,--interactive    verlange vor Abbruch Bestätigung des Benutzers\n"
"  -l,--list           liste alle bekannten Signalnamen auf\n"
"  -q,--quiet          keine Warnungen und Fehler ausgeben\n"
"  -r,--regexp         NAME ist ein erweiteter regulärer Ausdruck\n"
"  -s,--signal SIGNAL  sende benutzerdefiniertes Signal anstatt SIGTERM\n"
"  -u,--user USER      nur Prozesse von angegebenem Benutzer abbrechen\n"
"  -v,--verbose        berichte, falls das Signal erfolgreich gesendet wurde\n"
"  -V,--version        zeige Version\n"
"  -w,--wait           warte auf das Ende der Prozesse\n"

#: src/killall.c:688
#, c-format
msgid ""
"  -Z,--context REGEXP kill only process(es) having context\n"
"                      (must precede other arguments)\n"
msgstr ""
"  -Z,--context REGEXP breche nur Prozesse ab, die einen Kontext haben\n"
"                        (muss vor anderen Argumenten stehen)\n"

#: src/killall.c:700
#, fuzzy, c-format
msgid ""
"Copyright (C) 1993-2014 Werner Almesberger and Craig Small\n"
"\n"
msgstr ""
"Copyright (C) 1993-2010 Werner Almesberger und Craig Small\n"
"\n"

#: src/killall.c:790 src/killall.c:796
msgid "Invalid time format"
msgstr "Ungültiges Zeitformat"

#: src/killall.c:816
#, c-format
msgid "Cannot find user %s\n"
msgstr "Kann Benutzer %s nicht finden!\n"

#: src/killall.c:847
#, c-format
msgid "Bad regular expression: %s\n"
msgstr "Ungültiger regulärer Ausdruck: %s\n"

#: src/killall.c:879
#, c-format
msgid "killall: Maximum number of names is %d\n"
msgstr "killall: Die maximale Anzahl von Namen ist %d\n"

#: src/killall.c:884
#, c-format
msgid "killall: %s lacks process entries (not mounted ?)\n"
msgstr ""
"killall: Bei %s fehlen die Prozesseinträge (Dateisystem möglicherweise nicht "
"eingehängt?)\n"

#: src/peekfd.c:102
#, c-format
msgid "Error attaching to pid %i\n"
msgstr "Fehler beim Anhängen an PID %i\n"

#: src/peekfd.c:110
#, c-format
msgid "peekfd (PSmisc) %s\n"
msgstr "peekfd (PSmisc) %s\n"

#: src/peekfd.c:112
#, c-format
msgid ""
"Copyright (C) 2007 Trent Waddington\n"
"\n"
msgstr ""
"Copyright (C) 2007 Trent Waddington\n"
"\n"

#: src/peekfd.c:122
#, c-format
msgid ""
"Usage: peekfd [-8] [-n] [-c] [-d] [-V] [-h] <pid> [<fd> ..]\n"
"    -8 output 8 bit clean streams.\n"
"    -n don't display read/write from fd headers.\n"
"    -c peek at any new child processes too.\n"
"    -d remove duplicate read/writes from the output.\n"
"    -V prints version info.\n"
"    -h prints this help.\n"
"\n"
"  Press CTRL-C to end output.\n"
msgstr ""
"Aufruf: peekfd [-8] [-n] [-c] [-d] [-V] [-h] <pid> [<fd> ..]\n"
"    -8 8-Bit-Ausgabe erzwingen.\n"
"    -n Header für Lesen/Schreiben aus FDs nicht anzeigen.\n"
"    -c Auch nach neuen Kindprozessen Ausschau halten.\n"
"    -d Doppeltes Lesen/Schreiben nicht ausgeben.\n"
"    -V Versionsinformationen ausgeben.\n"
"    -h Diese Hilfe ausgeben.\n"
"\n"
"  Drücken Sie Strg-C, um die Ausgabe zu beenden.\n"

#: src/prtstat.c:54
#, c-format
msgid ""
"Usage: prtstat [options] PID ...\n"
"       prtstat -V\n"
"Print information about a process\n"
"    -r,--raw       Raw display of information\n"
"    -V,--version   Display version information and exit\n"
msgstr ""
"Aufruf: prtstat [Optionen] PID ...\n"
"        prtstat -V\n"
"Informationen über einen Prozess ausgeben\n"
"    -r,--raw       Rohe Ausgabe der Informationen\n"
"    -V,--version   Versionsinformationen anzeigen und beenden\n"

#: src/prtstat.c:65
#, c-format
msgid "prtstat (PSmisc) %s\n"
msgstr "prtstat (PSmisc) %s\n"

#: src/prtstat.c:66
#, c-format
msgid ""
"Copyright (C) 2009 Craig Small\n"
"\n"
msgstr ""
"Copyright (C) 2009 Craig Small\n"
"\n"

#: src/prtstat.c:78
msgid "running"
msgstr "läuft"

#: src/prtstat.c:80
msgid "sleeping"
msgstr "schläft"

#: src/prtstat.c:82
msgid "disk sleep"
msgstr "schläft (Disk)"

#: src/prtstat.c:84
msgid "zombie"
msgstr "zombie"

#: src/prtstat.c:86
msgid "traced"
msgstr "schritt"

#: src/prtstat.c:88
msgid "paging"
msgstr "auslagerung"

#: src/prtstat.c:90
msgid "unknown"
msgstr "unbekannt"

#: src/prtstat.c:164
#, c-format
msgid ""
"Process: %-14s\t\tState: %c (%s)\n"
"  CPU#:  %-3d\t\tTTY: %s\tThreads: %ld\n"
msgstr ""
"Prozess: %-14s\t\tZustand: %c (%s)\n"
"  CPU#:  %-3d\t\tTTY: %s\tThreads: %ld\n"

#: src/prtstat.c:169
#, c-format
msgid ""
"Process, Group and Session IDs\n"
"  Process ID: %d\t\t  Parent ID: %d\n"
"    Group ID: %d\t\t Session ID: %d\n"
"  T Group ID: %d\n"
"\n"
msgstr ""
"Prozess-, Gruppen- und Session-IDs\n"
"  Prozess-ID: %d\t\t  Eltern-ID: %d\n"
"  Gruppen-ID: %d\t\t Session-ID: %d\n"
"T-Gruppen-ID: %d\n"
"\n"

#: src/prtstat.c:175
#, c-format
msgid ""
"Page Faults\n"
"  This Process    (minor major): %8lu  %8lu\n"
"  Child Processes (minor major): %8lu  %8lu\n"
msgstr ""
"Seitenfehler\n"
"  Dieser Prozess  (klein groß): %8lu  %8lu\n"
"  Kindprozesse    (klein groß): %8lu  %8lu\n"

#: src/prtstat.c:180
#, c-format
msgid ""
"CPU Times\n"
"  This Process    (user system guest blkio): %6.2f %6.2f %6.2f %6.2f\n"
"  Child processes (user system guest):       %6.2f %6.2f %6.2f\n"
msgstr ""
"CPU-Zeiten\n"
"  Dieser Prozess  (user system guest blkio): %6.2f %6.2f %6.2f %6.2f\n"
"  Kindprozesse    (user system guest):       %6.2f %6.2f %6.2f\n"

#: src/prtstat.c:189
#, c-format
msgid ""
"Memory\n"
"  Vsize:       %-10s\n"
"  RSS:         %-10s \t\t RSS Limit: %s\n"
"  Code Start:  %#-10lx\t\t Code Stop:  %#-10lx\n"
"  Stack Start: %#-10lx\n"
"  Stack Pointer (ESP): %#10lx\t Inst Pointer (EIP): %#10lx\n"
msgstr ""
"Speicher\n"
"  Vsize:       %-10s\n"
"  RSS:         %-10s \t\t RSS-Grenzwert: %s\n"
"  Code-Start:  %#-10lx\t\t Code-Ende:  %#-10lx\n"
"  Stack-Start: %#-10lx\n"
"  Stackzeiger (ESP): %#10lx\t Befehlszeiger (EIP): %#10lx\n"

#: src/prtstat.c:199
#, c-format
msgid ""
"Scheduling\n"
"  Policy: %s\n"
"  Nice:   %ld \t\t RT Priority: %ld %s\n"
msgstr ""
"Prozessplanung\n"
"  Richtlinie: %s\n"
"  Nett:   %ld \t\t RT-Priorität: %ld %s\n"

#: src/prtstat.c:220
msgid "asprintf in print_stat failed.\n"
msgstr "asprintf in print_stat fehlgeschlagen.\n"

#: src/prtstat.c:225
#, c-format
msgid "Process with pid %d does not exist.\n"
msgstr "Prozess mit PID %d existiert nicht.\n"

#: src/prtstat.c:227
#, c-format
msgid "Unable to open stat file for pid %d (%s)\n"
msgstr "Konnte Stat-Datei für PID %d nicht öffnen: %s\n"

#: src/prtstat.c:311
msgid "Invalid option"
msgstr "Ungültige Option"

#: src/prtstat.c:316
msgid "You must provide at least one PID."
msgstr "Sie müssen mindestens eine PID angeben."

#: src/prtstat.c:320
#, c-format
msgid "/proc is not mounted, cannot stat /proc/self/stat.\n"
msgstr "/proc ist nicht eingehängt, kann /proc/self/stat nicht lesen.\n"

#: src/pstree.c:984
#, c-format
msgid "%s is empty (not mounted ?)\n"
msgstr "%s ist leer (Dateisystem möglicherweise nicht eingebunden?)\n"

#: src/pstree.c:1016
#, fuzzy, c-format
msgid ""
"Usage: pstree [ -a ] [ -c ] [ -h | -H PID ] [ -l ] [ -n ] [ -p ] [ -g ] [ -"
"u ]\n"
"              [ -A | -G | -U ] [ PID | USER ]\n"
"       pstree -V\n"
"Display a tree of processes.\n"
"\n"
"  -a, --arguments     show command line arguments\n"
"  -A, --ascii         use ASCII line drawing characters\n"
"  -c, --compact       don't compact identical subtrees\n"
"  -h, --highlight-all highlight current process and its ancestors\n"
"  -H PID,\n"
"  --highlight-pid=PID highlight this process and its ancestors\n"
"  -g, --show-pgids    show process group ids; implies -c\n"
"  -G, --vt100         use VT100 line drawing characters\n"
"  -l, --long          don't truncate long lines\n"
"  -n, --numeric-sort  sort output by PID\n"
"  -N type,\n"
"  --ns-sort=type      sort by namespace type (ipc, mnt, net, pid, user, "
"uts)\n"
"  -p, --show-pids     show PIDs; implies -c\n"
"  -s, --show-parents  show parents of the selected process\n"
"  -S, --ns-changes    show namespace transitions\n"
"  -t, --thread-names  show full thread names\n"
"  -u, --uid-changes   show uid transitions\n"
"  -U, --unicode       use UTF-8 (Unicode) line drawing characters\n"
"  -V, --version       display version information\n"
msgstr ""
"Aufruf: pstree [ -a ] [ -c ] [ -h | -H PID ] [ -l ] [ -n ] [ -p ] [ -g ] [ -"
"u ]\n"
"              [ -A | -G | -U ] [ PID | USER ]\n"
"        pstree -V\n"
"Zeigt einen Prozessbaum an.\n"
"\n"
"    -a, --arguments       Kommandozeilenargumente anzeigen\n"
"    -A, --ascii           ASCII-Zeichen für die Ausgabe benutzen\n"
"    -c, --compact         Identische Unterbäume nicht zusammenfassen\n"
"    -h, --highlight all   Aktuellen Prozess und seine Ahnen hervorheben\n"
"    -H PID,\n"
"    --highlight-pid=PID   Angegebenen Prozess und seine Ahnen hervorheben\n"
"    -g, --show-pgids      Prozessgruppen-IDs anzeigen; impliziert -c\n"
"    -G, --vt100           VT100-Zeichen für die Ausgabe benutzen\n"
"    -l, --long            Lange Zeilen nicht abschneiden\n"
"    -n, --numeric-sort    Ausgabe nach PID sortieren\n"
"    -N Art,\n"
"    --ns-sort=type        Nach Art des Namensraums sortieren (ipc, mnt, net, "
"pid, user, uts)\n"
"    -p, --show-pids       PIDs anzeigen; impliziert -c\n"
"    -s, --show-parents    Eltern des ausgewählten Prozesses anzeigen\n"
"    -S, --ns-changes      Wechsel des Namensraums anzeigen\n"
"    -u, --uid-changes     Wechsel der UID anzeigen\n"
"    -U, --unicode         UTF-8 (Unicode) für die Ausgabe verwenden\n"
"    -V, --version         Verwendete Version ausgeben\n"

#: src/pstree.c:1040
#, c-format
msgid "  -Z     show         SELinux security contexts\n"
msgstr "    -Z     zeige SELinux-Sicherheitskontext\n"

#: src/pstree.c:1042
#, c-format
msgid ""
"  PID    start at this PID; default is 1 (init)\n"
"  USER   show only trees rooted at processes of this user\n"
"\n"
msgstr ""
"    PID       mit dieser PID starten; Vorgabewert ist 1 (init)\n"
"    BENUTZER  zeige nur Prozessbäume, deren Wurzeln Prozesse dieses "
"Benutzers sind\n"
"\n"

#: src/pstree.c:1049
#, c-format
msgid "pstree (PSmisc) %s\n"
msgstr "pstree (PSmisc) %s\n"

#: src/pstree.c:1052
#, c-format
msgid ""
"Copyright (C) 1993-2009 Werner Almesberger and Craig Small\n"
"\n"
msgstr ""
"Copyright (C) 1993-2009 Werner Almesberger and Craig Small\n"
"\n"

#: src/pstree.c:1167
#, c-format
msgid "TERM is not set\n"
msgstr "TERM ist nicht gesetzt\n"

#: src/pstree.c:1171
#, c-format
msgid "Can't get terminal capabilities\n"
msgstr "Kann die Fähigkeiten des Terminals nicht erkennen\n"

#: src/pstree.c:1189
#, c-format
msgid "procfs file for %s namespace not available\n"
msgstr "procfs-Datei für Namensraum »%s« nicht erreichbar\n"

#: src/pstree.c:1239
#, c-format
msgid "No such user name: %s\n"
msgstr "Kein Benutzer mit dem Namen »%s«\n"

#: src/pstree.c:1265
#, c-format
msgid "No processes found.\n"
msgstr "Keine Prozesse gefunden.\n"

#: src/pstree.c:1271
#, c-format
msgid "Press return to close\n"
msgstr "Drücken Sie Enter zum Schließen\n"

#: src/signals.c:84
#, c-format
msgid "%s: unknown signal; %s -l lists signals.\n"
msgstr "%s: unbekanntes Signal; %s -l listet die Signale auf.\n"

#~ msgid ""
#~ "Copyright (C) 1993-2012 Werner Almesberger and Craig Small\n"
#~ "\n"
#~ msgstr ""
#~ "Copyright (C) 1993-2012 Werner Almesberger und Craig Small\n"
#~ "\n"

#~ msgid ""
#~ "Usage: pidof [ -eg ] NAME...\n"
#~ "       pidof -V\n"
#~ "\n"
#~ "    -e      require exact match for very long names;\n"
#~ "            skip if the command line is unavailable\n"
#~ "    -g      show process group ID instead of process ID\n"
#~ "    -V      display version information\n"
#~ "\n"
#~ msgstr ""
#~ "Aufruf: pidof [-eg] NAME...\n"
#~ "        pidof -V\n"
#~ "\n"
#~ "    -e   exakte Übereinstimmung für sehr lange Namen erforderlich;\n"
#~ "           ignorieren, wenn die Kommandozeile nicht verfügbar ist\n"
#~ "    -g   zeige Prozessgruppen-ID statt der Prozess-ID\n"
#~ "    -V   zeige Version\n"
#~ "\n"

#~ msgid ""
#~ "Copyright (C) 1993-2005 Werner Almesberger and Craig Small\n"
#~ "\n"
#~ msgstr ""
#~ "Copyright (C) 1993-2005 Werner Almesberger und Craig Small\n"
#~ "\n"

#~ msgid "You cannot use the mounted and mountpoint flags together"
#~ msgstr ""
#~ "Die Flags gemounted und mount-Punkt können nicht gleichzeitig verwendet "
#~ "werden."

#~ msgid "Cannot stat mount point %s: %s\n"
#~ msgstr "Kann Status von Einhängepunkt \"%s\" nicht ermitteln: %s\n"

#~ msgid "Cannot open /etc/mtab: %s\n"
#~ msgstr "/etc/mtab kann nicht geöffnet werden: %s\n"
